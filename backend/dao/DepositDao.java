@Repository
public interface DepositDao extends JpaRepository<Deposit, Long> {
}