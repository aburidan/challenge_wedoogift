@Repository
public interface CompanyDao extends JpaRepository<Company, Long> {
}