@Repository
public interface UserDao extends JpaRepository<User, Long> {
}