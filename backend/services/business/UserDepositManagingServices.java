public interface UserDepositManagingServices {

    /**
     * Calculates the user's balance from the deposits that the user received and that are still valid
     *
     * @param userId the id of the user
     * @return the balance of the user
     */
    long calculateUserBalance(long userId);

    /**
     * Gives a user a deposit from a company, if the balance allows it.
     * Tha balance of the company is updated after the deposit is given to the user.
     *
     * @param depositType the type of the deposit (meal or gift)
     * @param depositValue the amount of the deposit
     * @param companyId the id of the company
     * @param userId the id of the user
     * @return
     */
    boolean giveDepositToUser(CodeTypeDeposit depositType, long depositValue, long companyId, long userId);

}