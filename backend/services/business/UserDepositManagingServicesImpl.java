@Service
@TransactionalWithRollback
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class UserDepositManagingServicesImpl implements UserDepositManagingServices{

    private final CompanyDao companyDao;
    private final UserDao userDao;

    @Override
    public long calculateUserBalance(long userId) {
        User user = userDao.getById(userId);

        List<Deposit> allDepositList = user.getMealDeposits();
        allDepositList.addAll(user.getGiftDeposits());

        long userBalance = 0L;
        for (Deposit deposit: allDepositList) {
            userBalance = userBalance + deposit.getValue();
        }

        return userBalance;
    }

    @Override
    public boolean giveDepositToUser(CodeTypeDeposit depositType, long depositValue, long companyId, long userId) {
        Company company = companyDao.getById(companyId);
        User user = userDao.getById(userId);

        long companyBalance = company.getBalance();
        if ((companyBalance - depositValue) >= 0) {
            Deposit deposit = new Deposit();
            deposit.setType(depositType);
            deposit.setValue(depositValue);
            deposit.setEmissionDate(LocalDate.now());

            user.getDepositList().add(deposit);

            company.setBalance(companyBalance - depositValue);

            // saving the company will also wave the user, which will cascade and save the deposit as well
            companyDao.save(company);

            return true;
        }
        return false;
    }

}