@Entity
@Table(name = "DEPOSIT")
@Getter
@Setter
public class Deposit {

    @Id
    @Column(name = "DEPOSIT_ID")
    @SequenceGenerator(name = "S_DEPOSIT", sequenceName = "S_DEPOSIT", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_DEPOSIT")
    private Long id;

    @NotNull
    @Column(name = "VALUE")
    private long value;

    @NotNull
    @Column(name = "EMISSION_DATE")
    private LocalDate emissionDate;

    @NotNull
    @Column(name = "TYPE")
    protected CodeTypeDeposit type;

    @Transient
    public boolean isMealDeposit() {
        return this.type.equals(CodeTypeDeposit.MEAL);
    }

    @Transient
    public boolean isGiftDeposit() {
        return this.type.equals(CodeTypeDeposit.GIFT);
    }

    /**
     * Indicates whether the deposit is still valid or not, depending on its type (meal or gift)
     *
     * @return true if the deposit can still be used, false otherwise
     */
    @Transient
    public boolean isStillValid() {
        final LocalDate today = LocalDate.now();
        if (isGiftDeposit()) {
            return !today.isAfter(this.emissionDate.plusDays(365));
        } else if (isMealDeposit()) {
            final int currentYear = today.getYear();
            final int emissionYear = this.emissionDate.getYear();
            final boolean isSameYear = emissionYear == currentYear;
            final boolean isTodayBeforeMarchNextYear = (currentYear == emissionYear+1) && (today.getMonthValue() < 3);
            return (isSameYear || isTodayBeforeMarchNextYear);
        } else {
            return false;
        }
    }
}