@Entity
@Table(name = "USER")
@Getter
@Setter
public class User {

    @Id
    @Column(name = "USER_ID")
    @SequenceGenerator(name = "S_USER", sequenceName = "S_USER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_USER")
    private Long id;

    @NotNull
    @Column(name = "FIRST_NAME")
    private String lastName;

    @NotNull
    @Column(name = "LAST_NAME")
    private String firstName;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "DEPOSIT_ID")
    private List<Deposit> depositList;

    @Transient
    public List<Deposit> getMealDeposits() {
        return this.depositList.stream().filter(deposit -> deposit.isMealDeposit() && deposit.isStillValid()).collect(Collectors.toList());
    }

    @Transient
    public List<Deposit> getGiftDeposits() {
        return this.depositList.stream().filter(deposit -> deposit.isGiftDeposit() && deposit.isStillValid()).collect(Collectors.toList());
    }

}