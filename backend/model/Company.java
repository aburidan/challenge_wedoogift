@Entity
@Table(name = "COMPANY")
@Getter
@Setter
public class Company {

    @Id
    @Column(name = "COMPANY_ID")
    @SequenceGenerator(name = "S_COMPANY", sequenceName = "S_COMPANY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_COMPANY")
    private Long id;

    @NotNull
    @Column(name = "BALANCE")
    private long balance;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "USER_ID")
    private List<User> userList;

}